var tablinks = document.getElementsByClassName("tab-links");
var tabcontents = document.getElementsByClassName("tab-contents");
var sidemenu = document.getElementById("sidemenu");
var navItems = document.querySelectorAll('#sidemenu a');

function opentab(tabname){
    for(tablink of tablinks){
    tablink.classList.remove("active-link")
}
for(tabcontent of tabcontents){
    tabcontent.classList.remove("active-tab")
}
event.currentTarget.classList.add("active-link")
document.getElementById(tabname).classList.add("active-tab")
}

 function openmenu(){
      sidemenu.style.right = "0";
      }

      function closemenu(){
        sidemenu.style.right = "-200";
      }

      document.addEventListener("DOMContentLoaded", function() {
      
       navItems.forEach(function(item) {
          item.addEventListener('click', function() {
            navItems.forEach(function(item) {
              item.classList.remove('active');
            });
             this.classList.add('active');
          });
        });
      });
